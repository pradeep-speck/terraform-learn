# For admin user, provider is a program that knows how to talk to api
# credentials are not be push to git repo, used for demo purpose
provider "aws" {
  region     = "ap-south-1"
#   access_key = "access_key_id"
#   secret_key = "secret_access_key"
}
# These values are obtained when IAM user creation in excel sheet
# Use environment variables:
# $ export AWS_ACCESS_KEY_ID="anaccesskey"
# $ export AWS_SECRET_ACCESS_KEY="asecretkey"
# $ export AWS_DEFAULT_REGION="us-west-2"

# OR configure aws credentials on local system
# check credentials using command $ aws configure list
# change values if not applicable using command $ aws configure


# resource is used to create a resource in infrastructure
# data is used to refer existing data sources.

# development_vpc is terraform context name
resource "aws_vpc" "development_vpc" {
# cidr_block = var.cidr-block-vpc
# cidr_block = var.cidr-blocks[0]
cidr_block = var.cidr-blocks[0].cidr-block
tags = {
    # Name : "development"
    Name : var.cidr-blocks[0].name
    # Name is used to define name of resource, here vpc resource
    # tags help in defining additional key, value pairs, with or without Name key
}    
}


variable "cidr-blocks" {
description = "cidr blocks for vpc and subnet"
type = list(object({
    cidr-block = string,
    name = string
}))
}

# variable "cidr-blocks" {
# description = "array of cidr blocks"
# type = list(string)
# }

# variable "cidr_block_subnet" {
#     description = "subnet's cidr block"
#     type = string 
# }

# variable "cidr-block-vpc" {
# type = string
# description = "vpc's cidr block"
# default = "10.0.0.0/16"
# }

# environment variable TF_VAR_avail_zone="ap-south-1a" shall be set in terminal -
#  - to provide value to avail_zone variable, if env variable is not provided, a user prompt for variable value will be asked.
variable "avail_zone" {
}

resource "aws_subnet" "dev-subnet-1" {
vpc_id = aws_vpc.development_vpc.id
# cidr_block = "10.0.0.0/24"
# cidr_block = var.cidr_block_subnet
# cidr_block = var.cidr-blocks[1]
cidr_block = var.cidr-blocks[1].cidr-block
availability_zone = var.avail_zone
tags = {
    # Name : "subnet-1-dev"
    Name : var.cidr-blocks[1].name
}
}

# data is used to refer existing data sources.
# resource is used to create a resource in infrastructure

# Commented to remove below resource.

# data "aws_vpc" "existing_vpc" {
#  default = true
# }

# resource "aws_subnet" "dev-subnet-2" {
#  vpc_id = data.aws_vpc.existing_vpc.id
#  cidr_block = "172.31.48.0/20"
#  availability_zone = "ap-south-1a"
#  tags = {
#      Name : "subnet-2-default"
#  }
# }


output "dev-vpc-id" {
  value  =  aws_vpc.development_vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}


